"""
implement_3.py
Implements the recipe no 3.
3 angles, 1 edge length.
"""

import pickle
from random import randint
from sys import argv
from corefuncs import read
from corefuncs import generate
from corefuncs import interpret
from helperfuncs import sinusthm
from helperfuncs import pstopoint
from helperfuncs import degtorad
from elements import Point
# from random import randint
# from math import pi
# from random import random

# LOADING PREVIOUS LOFE.
with open('LOFE.pickle', 'rb') as f:
    PREV_LOFE = pickle.load(f)
# LOADING PREVIOUS LOFE.

#
alphabet = 'ABCDEFGHIJKLMNOPRSTUVYZ'
l_of_letters = [i for i in alphabet]
#

# FINDING APPROPRIATE NAMES
l_of_names = []
while len(l_of_names) < 3:
    letter = l_of_letters[randint(0, len(l_of_letters)-1)]
    if letter not in [i.name for i in PREV_LOFE\
            if 'name' in i.__dict__]:
        if letter not in l_of_names:
            l_of_names.append(letter)
# FINDING APPROPRIATE NAMES

## ***testing
#print(l_of_names) # Succesful.
## ***testing

### INPUT ###
BC_LEN = float(argv[1])
ABC_VAL = degtorad(float(argv[2]))
BCA_VAL = degtorad(float(argv[3]))
CAB_VAL = degtorad(float(argv[4]))
### INPUT ###

# Storing original namenclature for sanity.
"""
L_OF_INP_1 = [
    'point A 0 0',
    'point C',
    'point B',
    'segment BC {}'.format(BC_LEN),
    'angle ABC {}'.format(ABC_VAL),
    'angle BCA {}'.format(BCA_VAL),
    'angle CAB {}'.format(CAB_VAL)
    ]

L_OF_INP_2 = ['triangle ABC']
"""
# Storing original namenclature for sanity.

# For shorten the line lengths
# We are going to define compound names
# BC := 'B'+'C' etc.
BC_NAME = l_of_names[2]+l_of_names[1]
ABC_NAME = l_of_names[0]+l_of_names[2]+l_of_names[1]
BCA_NAME = l_of_names[2]+l_of_names[1]+l_of_names[0]
CAB_NAME = l_of_names[1]+l_of_names[0]+l_of_names[2]
# --- #
AB_NAME = l_of_names[0]+l_of_names[2]
AC_NAME = l_of_names[0]+l_of_names[1]
A_NAME = l_of_names[0]
C_NAME = l_of_names[1]
B_NAME = l_of_names[2]
# Compound names done.

# We're going to predefine the commands
# for 'read' function.
L_OF_INP_1 = [
    'point {} 0 0'.format(A_NAME),
    'point {}'.format(C_NAME),
    'point {}'.format(B_NAME),
    'segment {} {}'.format(BC_NAME, BC_LEN),
    'angle {} {}'.format(ABC_NAME, ABC_VAL),
    'angle {} {}'.format(BCA_NAME, BCA_VAL),
    'angle {} {}'.format(CAB_NAME, CAB_VAL)
    ]

L_OF_INP_2 = ['triangle {}'.format(ABC_NAME)]
# Predefinition done.

def bold_wrapper(inp, lofe):
    """
    For being able to store the elements
    list. (Interpret doesn'r return the list.
    So a function that returns it is needed.)
    """
    interpret(inp, lofe)
    return lofe


###
LOFE = []
for i in L_OF_INP_1:
    very = read(i)
    LOFE = bold_wrapper(very, LOFE)
###

AB = sinusthm([LOFE[3], LOFE[-1]], LOFE[-2], AB_NAME, LOFE)
AC = sinusthm([LOFE[3], LOFE[-1]], LOFE[-3], AC_NAME, LOFE)

LOFE.append(AB)
LOFE.append(AC)

C = Point(C_NAME, [], AC.length, 0)
B = pstopoint(LOFE[0], LOFE[-2], LOFE[-3].value, B_NAME)
print('The updated B is: ' + str(B))
print('The updated C is: ' + str(C))
print(LOFE, 'BEFORE')
LOFE[2].update_point(B, LOFE) # Point 'B' was bold. It is updated.
LOFE[1].update_point(C, LOFE) # Point 'C' was bold. It is updated.
print('Update successful.')
print(LOFE, 'AFTER')

###
LOFE = bold_wrapper(read(L_OF_INP_2[0]), LOFE)
###

#generate(LOFE[-1], l_of_elements=LOFE)
# See the file 'out.tex' for the output.

# Pickling the LOFE for later usage.
with open('LOFE.pickle', 'wb') as f:
    pickle.dump(LOFE, f, pickle.HIGHEST_PROTOCOL)

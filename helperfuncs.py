"""
helperfuncs.py
Contains helper functions.

Note: test_ prefix to functions makes them
avaliable to testing.
"""

from math import sin
from math import cos
from math import pi
from math import acos
from math import sqrt
#import pytest
from elements import Point
from elements import Angle
from elements import Segment

# This is for test purposes.
class BoldSegment:
    """
    BoldSegment class, can be used for
    tests without concerning point dependencies.
    """
    def __init__(self, name, length):
        self.name = name
        self.length = length

    def __repr__(self):
        return 'BoldSegment({}, {})'.format(self.name, self.length)
# This is for test purposes.

#A = Point('A', [], 0, 0)
#S = BoldSegment(2, 'S')
#ARGLIST = [(A, S, pi/3, 'D')]
#@pytest.mark.parametrize('point, segment, angle, name', ARGLIST)
def pstopoint(point, segment, angle, name):
    """
    Generic point creator, takes a point,
    a segment and an angle. Returns a point.

    Creation process:
    point: Center of the circle.
    segment: Radius of the circle.
    angle: Positive 'angle' angle.

            -----
        ---       *--
    ---          /     ---
                / angle
-------------- * -------------

    ---               ---
        ---       ---
            -----

    """
    x_val = float(point.x_val)
    y_val = float(point.y_val)
    length = float(segment.length)
    angle = float(angle)
    new_x = x_val + length*cos(angle)
    # assert fltred(x + length*cos(angle)) == 1
    new_y = y_val + length * cos(pi/2 - angle)
    # assert fltred(x + length*cos(pi/2 - angle)) == fltred(sqrt(3))
    return Point(name, [], fltred(new_x), fltred(new_y))

def cosinusthm(l_of_params, type_of_opr, name, l_of_elements=None):
    """
    Implementation of cosinus theorem.
    Returns segment or angle object
    regarding to type_of_opr.

    l_of_params: List of parameters. Either
    [b, c, angle] or [b, c, a].

    type_of_opr: Determines the direction
    of the operation. Either 'forward' or 'backward'.

    name: Name of the element that is going
    to be returned.

    l_of_elements: List of elements that
    is going to be provided.
    """

    if l_of_elements is None:
        l_of_elements = []

    if type_of_opr == 'forward':
        b_gth = l_of_params[0].length
        c_gth = l_of_params[1].length
        angle = l_of_params[2].value

        val = sqrt(b_gth**2 + c_gth**2 - 2*b_gth*c_gth*cos(angle))
        val = fltred(val)
        return Segment(name, l_of_elements, val)
    elif type_of_opr == 'backward':
        b_gth = l_of_params[0].length
        c_gth = l_of_params[1].length
        a_gth = l_of_params[2].length

        val = acos((b_gth**2 + c_gth**2 - a_gth**2) / (2*b_gth*c_gth))
        val = fltred(val)
        return Angle(name, l_of_elements, val)

    return None

def sinusthm(pair, intent_angle, name, l_of_elements=None):
    """
    Sinus theorem implementation.
    Returns segment object.

    pair: The reference angle-point pair
    that is going to be used for calculation.

    intent_angle: The angle object which is located
    in the opposite of the intended segment.

    name: Name of the element that is going
    to be returned.

    l_of_elements: List of elements that
    is going to be provided.
    """
    if l_of_elements is None:
        l_of_elements = []

    pair_segment = pair[0]
    pair_angle = pair[1]

    intent_s_length = sin(float(intent_angle.value)) *\
            (float(pair_segment.length) / sin(float(pair_angle.value)))

    return Segment(name, l_of_elements, intent_s_length)

def fltred(flt, digit=5):
    """
    We presume that type(flt) is float.

    flt: Float number that will be reduced.
    """
    #FIXME: 6.661338147750939e-16 will be reduced to 6.66133.
    temp = str(flt)
    landr = temp.split('.')
    return float(landr[0] + '.' + landr[1][:digit])

def degtorad(deg):
    """
    Converts degrees to radians.
    """
    return (pi / 180) * deg

def lenchecker(itr):
    """
    Checks whether the length of the
    objects of an iterable is the same or not.

    itr: An iterable object like list or tuple.
    """
    for i in range(len(itr) - 1):
        #print('{} ~ {}'.format(i, i+1)) # Put for debugging purposes.
        if len(itr[i]) != len(itr[i+1]):
            return False

    return True

def shift(point, vector):
    v_x, v_y = vector.t_of_comp

    p_x = point.x_val
    p_y = point.y_val

    new_x = v_x + p_x
    new_y = v_y + p_y

    return Point(point.name, [], new_x, new_y)

def rotate_base(point, angle):
    """
    Rotates the point around origin
    'angle' degrees.

    point: The point that will be rotated.
    angle: Rotation amount in degrees.
    """
    # Importing Vector and Matrix
    # from matrix module.
    from matrix import Vector
    from matrix import Matrix

    # Converting degrees to radians.
    angle = degtorad(angle)

    # Defining a rotation matrix.
    v_1 = Vector(cos(angle), sin(angle))
    v_2 = Vector(-sin(angle), cos(angle))
    r_mat = Matrix([v_1, v_2])

    # Extracting point attr to a matrix
    # for multiplication.
    p_v = Vector(point.x_val, point.y_val)
    p_mat = Matrix([p_v])

    mul_mat = p_mat * r_mat
    new_x = mul_mat.l_of_vectors[0].t_of_comp[0]
    new_y = mul_mat.l_of_vectors[0].t_of_comp[1]

    return Point(point.name, point.l_of_elements, new_x, new_y)

def rotate(point, around, angle):
    """
    Rotates the point around 'around'
    'angle' degrees. HOW: Shifting the
    points to origin, rotating with
    the 'rotate' function, shifting
    back.

    point: The point that will be rotated.
    around: The rotation center.
    angle: Rotation amount in degrees.
    """
    from matrix import Vector

    # Initializing shifting vectors.
    l_of_comp = (around.x_val, around.y_val)
    back_vec = Vector(*l_of_comp)
    shift_vec = back_vec * (-1)
    # Initializing shifting vectors.

    # Shifting
    new_point = shift(point, shift_vec)
    # Shifting

    # Rotating
    rotated = rotate_base(new_point, angle)
    # Rotating

    # Shifting back.
    result = shift(rotated, back_vec)
    # Shifting back.

    return result

def merger_base(first, sec):
    """
    Merges two lists to one.
    """
    for i in sec:
        if i not in first:
            first.append(i)

    return first

def merger(*t_of_lists):
    """
    Merges arbitrary amount
    of lists to one.
    """
    new_list = []
    for i in t_of_lists:
        new_list = merger_base(new_list, i)

    return new_list

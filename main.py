"""
main.py
euclide-tkz code generator from input within a repl env.
"""

from corefuncs import interpret, read

def repl(lofe=None):
    """
    Provides repl environment, can be left
    by the return value of interpret function.
    """
    if lofe is None:
        lofe = []
    dec = True
    while dec:
        if interpret(read(), lofe) is False:
            dec = False
        while None in lofe:
            lofe.remove(None)
        print('Current content of elements list is {}'.format(lofe))
    return lofe

if __name__ == '__main__':
    repl() # Specify a non-empty list if needed.

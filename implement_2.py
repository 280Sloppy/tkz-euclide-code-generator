"""
implement_2.py
Implements the recipe no 2.
2 edge lengths, angle in the middle.
"""

from random import randint
import pickle
from sys import argv
from corefuncs import read
from corefuncs import generate
from corefuncs import interpret
from helperfuncs import degtorad
from helperfuncs import pstopoint
# from random import randint
# from math import pi
# from random import random

# LOADING PREVIOUS LOFE.
with open('LOFE.pickle', 'rb') as f:
    PREV_LOFE = pickle.load(f)
# LOADING PREVIOUS LOFE.

#
ALPHABET = 'ABCDEFGHIJKLMNOPRSTUVYZ'
L_OF_LETTERS = [i for i in ALPHABET]
#

# FINDING APPROPRIATE NAMES
L_OF_NAMES = []
while len(L_OF_NAMES) < 3:
    LETTER = L_OF_LETTERS[randint(0, len(L_OF_LETTERS)-1)]
    if LETTER not in [i.name for i in PREV_LOFE\
            if 'name' in i.__dict__]:
        if LETTER not in L_OF_NAMES:
            L_OF_NAMES.append(LETTER)
# FINDING APPROPRIATE NAMES

## ***testing
#print(L_OF_NAMES) # Succesful.
## ***testing

### INPUT ###
AB_LEN = float(argv[1])
AC_LEN = float(argv[2])
CAB_VAL = degtorad(float(argv[3]))
### INPUT ###

# Storing original nomenclature for sanity.
"""
L_OF_INP_1 = [
    'point A 0 0',
    'point C {} 0'.format(AC_LEN),
    'point B',
    'segment AB {}'.format(AB_LEN),
    'segment AC {}'.format(AC_LEN),
    'angle CAB {}'.format(CAB_VAL)
    ]

L_OF_INP_2 = ['triangle ABC']
"""
# Storing original nomenclature for sanity.

# For shorten the line lengths
# We are going to define compound names
# BC := 'B'+'C' etc.
BC_NAME = L_OF_NAMES[2]+L_OF_NAMES[1]
ABC_NAME = L_OF_NAMES[0]+L_OF_NAMES[2]+L_OF_NAMES[1]
CAB_NAME = L_OF_NAMES[1]+L_OF_NAMES[0]+L_OF_NAMES[2]
# --- #
AB_NAME = L_OF_NAMES[0]+L_OF_NAMES[2]
AC_NAME = L_OF_NAMES[0]+L_OF_NAMES[1]
A_NAME = L_OF_NAMES[0]
C_NAME = L_OF_NAMES[1]
B_NAME = L_OF_NAMES[2]
# Compound names done.

# We're going to predefine the commands
# for 'read' function.
L_OF_INP_1 = [
    'point {} 0 0'.format(A_NAME),
    'point {} {} 0'.format(C_NAME, AC_LEN),
    'point {}'.format(B_NAME),
    'segment {} {}'.format(AB_NAME, AB_LEN),
    'segment {} {}'.format(AC_NAME, AC_LEN),
    'angle {} {}'.format(CAB_NAME, CAB_VAL)
    ]

L_OF_INP_2 = ['triangle {}'.format(ABC_NAME)]
# Predefinition done.

def bold_wrapper(inp, lofe):
    """
    For being able to store the elements
    list. (Interpret doesn'r return the list.
    So a function that returns it is needed.)
    """
    interpret(inp, lofe)
    return lofe

###
LOFE = []
for i in L_OF_INP_1:
    very = read(i)
    LOFE = bold_wrapper(very, LOFE)
###

B = pstopoint(LOFE[0], LOFE[3], LOFE[5].value, B_NAME)
print('The updated B is: ' + str(B))
print(LOFE, 'BEFORE')
LOFE[2].update_point(B, LOFE) # Point 'B' was bold. It is updated.
print('Update successful.')
print(LOFE, 'AFTER')

### Definition of the triangle.
LOFE = bold_wrapper(read(L_OF_INP_2[0]), LOFE)
### Ready to be passed to 'generate'.

#generate(LOFE[-1], l_of_elements=LOFE)
# See the file 'out.tex' for the output.

# Pickling the LOFE for later usage.
with open('LOFE.pickle', 'wb') as f:
    pickle.dump(LOFE, f, pickle.HIGHEST_PROTOCOL)

#!/usr/bin/env bash


mkdir -p ./texworks
cat beginning.tex out.tex end.tex > ./texworks/result.tex
cd texworks
latex result.tex
dvisvgm -n result.dvi

#a=$(command -v firefox)
#if [ -f $a ]
#then
#    $a result.svg
#fi

b=$(command -v chromium)
if [ -f $b ]
then
    echo "Opening the resulting svg file on Chromium."
    $b result.svg
fi

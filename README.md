# `TeCG`
## Description
> A tool that accompanies code production for
[tkz-euclide](https://ctan.org/pkg/tkz-euclide) - a latex
package that draws geometric figures in LaTeX-.

## Table of Contents
1. Dependencies
2. Usage
3. Tasks
4. License

## Dependencies
Given that this part is going to be explained for archlinux at least for now,
TeCG is written in pure Python, particularly in Python 3.
So up to tex file creation, there is nothing needed except Python. After that,
 you should
[TexLive](https://www.archlinux.org/groups/x86_64/texlive-most/) have installed.
It comes with
`latex` `dvisvgm` commands that is used in `defragment.sh` file.
And of course, you should
[tkz-euclide](https://ctan.org/pkg/tkz-euclide) have installed,
but it probably comes with the package which is just
mentionned. So in short:
* Python 3
* TexLive
* tkz-euclide package

## Usage
Firstly, it is recommended to read `recipes.rdm`. It briefly explains the
basic constructions. `implement*.py` files are applications
of that recipe. So, you can externally create a py file to create such
applications or you can also use the `repl` env to
create elements interactively. Besides that, the main folder contains
`beginning.tex` and `end.tex` files. In conjunction
with the resulting `out.tex` file, `defragment.sh` creates a `result.tex`
file in `texworks` directory, produces a `dvi`
file from that and finally, converts it to a `svg` file. So presuming
you have `apply.py` written, you do:
```bash
python apply.py
./defragment.sh
```
This is what you'll get *as an example*

![Product](svgs/0.svg).

Noting that LOFE is the list of elements that you've defined;
this is how you define a point, segment and a triangle.

```python
O = Point('O', LOFE, 0, 0) # O(0, 0)
P = Point('P', LOFE, 3, 4) # P(3, 4)
K = Point('K', LOFE, 6, 0)

OP = Segment('OP', LOFE, 5)
OPK = Triangle('OPK', LOFE)
```

Resuming from the last example, we define the circumcircle
of a triangle:

```python
C_OPK = Circle(LOFE, tri=OPK)
```

For a relatively complex assembly, check out the blogpost in
[280sloppy.github.io](https://280sloppy.github.io/tecg-ile-bir-gogeometry-sorusu.html).

## Tasks
- [x] Adding new constructions to recipes.
- [x] Labeling segments.
- [ ] Placing labels (Point labels are the subject for now.) such
    that they don't overlap with segments etc.
- [x] Adding circle element.
- [ ] Adding line element.
- [x] Writing a rotation matrix for rotating elements.
- [x] Writing a vector function/class to shift polygons etc.
- [x] Seperating point definitions from triangle part of the generate function.
- [ ] Writing a reflection function.
- [ ] Seperating 'interpret' function (*simply the repl env.*) from corefuncs.py
- [ ] Modularizing (or somehow change the structure of) the 'interpret' function.
- [ ] Converting project to a package. Accordingly organizing imports.
- [ ] Organizing error classes. (This is needed due to my lack of understanding about
    custom error classes.)

## Proof of concept
The main goal of this project is to orient the workflow
of drawing geometric (euclidean geometry) figures with *tkz-euclide*
such that it is more interactive and is purged from the calculations.
Considering what I've started from, I think that one has to prove
that it is possible to draw all we want in the universe
of euclidean geometry (at least
a significant part of it) with what we have as framework.
Saying 'prove', I imply a mathematical proof. Additionnaly,
I have the intention to draw considerable amount of figures
with it to show that it is really feasible.

## License
TeCG licensed under [GPL-3.0.](LICENSE).

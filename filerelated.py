"""
filerelated.py
Contains functions related to file operations.
"""

import pickle

def wrap(strobj, wrapper, need_pairs=True):
    """
    Wraps the <elementobj.name> with the
    <wrapper>.
    """
    pairs = {'{': '}', '[': ']', '(': ')'}
    if need_pairs:
        return wrapper + strobj + pairs[wrapper]

    return wrapper + strobj + wrapper

def spacer():
    """
    Creates a empty line if out.tex
    """
    with open('out.tex', 'a') as fileobj:
        fileobj.write('\n')

def tri_name_init(tri):
    """
    Initialize the name of the
    triangle given to tkzDrawPolygon.

    So ABC evolves to A,B,C.
    """
    name = tri.name
    output_str = ''
    for i in name:
        output_str += (i+',')
    output_str = output_str[:-1]

    return output_str

def loadpickle():
    """
    Load last pickle file located
    at the same directory.
    """
    with open('LOFE.pickle', 'rb') as fileobj:
        prev_lofe = pickle.load(fileobj)

    return prev_lofe

"""
elements.py
Contains element classes.
"""

from math import pi
from errors import PointError, MultipleInstanceError

class Point:
    """
    Point element class.
    Contructs a point with 'name', 'x_val', 'y_val'.

    l_of_elements: list of elements.
    It is used to test whether the point already
    exists or not. So inside the class, the initialized
    point can't be seen.
    """
    def __init__(self, name, l_of_elements, x=None, y=None):
        self.name = name
        self.l_of_elements = l_of_elements
        self.wblabeled = True # Points are labeled by default.
        # Existence test
        for i in self.l_of_elements:
            # No need to check type of element
            # Since there is no other element
            # that has such name. (A single letter.)
            if i.name == self.name:
                raise MultipleInstanceError('{} already exists.'\
                        .format(self.name))
        # Existence test

        try:
            self.x_val = float(x)
            self.y_val = float(y)
        except (ValueError, TypeError):
            self.x_val = None
            self.y_val = None

    def __repr__(self):
        return '{}({}, {})'.format(self.name, self.x_val, self.y_val)

    def compare_names(self, other):
        """
        For comparing the point objects.
        """
        print('(DEBUG): ' + self.name + ',' + other.name)
        return self.name == other.name

    def update_point(self, very_point, lofe):
        """
        Updates the point attributes.
        """

        for i in lofe:
            print(self.name)
            if self.compare_names(i):
                lofe.remove(i)

        lofe.append(very_point)

    def label(self):
        """
        Changes the label state of
        the element. The default value
        for Point is True.
        """
        if self.wblabeled:
            self.wblabeled = False
        else:
            self.wblabeled = True


class Segment:
    """
    Segment element class.
    Constructs a segment by 'name' with
    checking the existence of the points in 'name'.

    l_of_elements: list of elements.
    length: the length of segment.
    """
    def __init__(self, name, l_of_elements, length=None):
        self.name = name
        self.l_of_elements = l_of_elements
        self.wblabeled = False # Segments aren't labeled by default.
        # Existence test
        for i in self.l_of_elements:
            if i.name == self.name:
                raise MultipleInstanceError('{} already exists.'.format(self.name))
        # Existence test

        try:
            self.length = float(length)
        except (ValueError, TypeError):
            self.length = None

        # Point dependency test
        self.p_letters = [i for i in name]
        for k in self.p_letters:
            if k not in [j.name for j in self.l_of_elements]:
                raise PointError('{} does not exist.'.format(k))
        # Point dependency test

    def __repr__(self):
        return '{}({})'.format(self.name, self.length)

    def __len__(self):
        return self.length

    def get_points_name(self):
        """
        Returns element's points
        with its names.
        """

        return self.p_letters

    def get_points_as_object(self):
        """
        Return element's points
        as objects.
        """
        p_list = list()
        for i in self.l_of_elements:
            for k in self.p_letters:
                if i.name == k:
                    p_list.append(i)
        return p_list

    def label(self):
        """
        Changes the label state of
        the element. The default value
        for Segment is False.
        """
        if self.wblabeled:
            self.wblabeled = False
        else:
            self.wblabeled = True

    def sft(self, vector):
        """
        Shift method for Segment class.
        Returns the shifted Segment with
        its updated l_of_elements.

        vector: The vector used for shifting.
        """
        from helperfuncs import shift

        l_of_points = self.get_points_as_object()
        shifted_points = [shift(i, vector) for i in l_of_points]

        l_of_elements = [i for i in self.l_of_elements if i is not self]

        for i in shifted_points:
            for k in l_of_elements:
                if isinstance(k, Point) and (i.name == k.name):
                    k.update_point(i, l_of_elements)

        return self.__class__(self.name, l_of_elements, self.length)

    def rot(self, around, angle):
        """
        Rotate function for the Segment class.
        Return the rotated Segment with
        its updated l_of_elements.

        around: The rotation center.
        angle: Rotation amount in degrees.
        """
        from helperfuncs import rotate

        l_of_points = self.get_points_as_object()
        rotated_points = [rotate(i, around, angle) for i in l_of_points]

        l_of_elements = [i for i in self.l_of_elements if i is not self]

        for i in rotated_points:
            for k in l_of_elements:
                if isinstance(k, Point) and (i.name == k.name):
                    k.update_point(i, l_of_elements)

        return self.__class__(self.name, l_of_elements, self.length)


class Angle:
    """
    Angle element class.
    Constructs a angle by 'name' with
    checking the existence of the points in 'name'.

    value: Value of angle, 3*pi/2 etc.
    l_of_elements: list of elements.
    """
    def __init__(self, name, l_of_elements, value):
        self.name = name
        self.l_of_elements = l_of_elements
        # Existence test
        for i in self.l_of_elements:
            if isinstance(i, Angle):
                if i.name == self.name:
                    raise MultipleInstanceError('{} already exists.'\
                            .format(self.name))
        # Existence test

        self.value = value
        self.value_deg = (180 * float(self.value)) / pi

        # Point dependency test
        self.p_letters = [i for i in name]
        for k in self.p_letters:
            if k not in [j.name for j in self.l_of_elements]:
                raise PointError('{} does not exist'.format(k))
        # Point dependency test

    def __repr__(self):
        return '{}({})'.format(self.name, self.value)

    def get_points_name(self):
        """
        Returns element's points
        with its names.
        """

        return self.p_letters

    def get_points_as_object(self):
        """
        Return element's points
        as objects.
        """

        p_list = list()
        for i in self.l_of_elements:
            for k in self.p_letters:
                if i.name == k:
                    p_list.append(i)
        return p_list

    def as_degrees(self):
        """
        Return the element's value
        as degrees.
        """
        return '{}({})'.format(self.name, self.value_deg)


class Triangle:
    """
    Triangle element class.
    Constructs a triangle by 'name' with
    checking the existence of the points in 'name'.

    l_of_elements: list of elements.
    """

    def __init__(self, name, l_of_elements):
        self.name = name
        self.l_of_elements = l_of_elements
        # Existence test
        for i in self.l_of_elements:
            if isinstance(i, Triangle):
                if i.name == self.name:
                    raise MultipleInstanceError('{} already exists.'\
                            .format(self.name))
        # Existence test

        # Point dependency test
        self.p_letters = [i for i in name]
        for k in self.p_letters:
            if k not in [j.name for j in self.l_of_elements]:
                raise PointError('{} does not exist'.format(k))
        # Point dependency test

    def __repr__(self):
        return 'Triangle({})'.format(self.name)

    def get_points_name(self):
        """
        Returns element's points
        with its names.
        """

        return self.p_letters

    def get_points_as_object(self):
        """
        Return element's points
        as objects.

        Noting that this returns points in exact order
        that the name has. Regardless of
        the change in order of the l_of_elements.
        """
        p_list = list()

        for i in self.p_letters:
            for k in self.l_of_elements:
                if isinstance(k, Point):
                    if i == k.name:
                        print('Appending the point {}'\
                                .format(k))
                        p_list.append(k)

        return p_list

    def sft(self, vector):
        """
        Shift method for Triangle class.
        Returns the shifted Triangle with
        its updated l_of_elements.

        vector: The vector used for shifting.
        """
        from helperfuncs import shift

        l_of_points = self.get_points_as_object()
        shifted_points = [shift(i, vector) for i in l_of_points]

        l_of_elements = [i for i in self.l_of_elements if i is not self]

        for i in shifted_points:
            for k in l_of_elements:
                if isinstance(k, Point) and (i.name == k.name):
                    k.update_point(i, l_of_elements)

        return self.__class__(self.name, l_of_elements)

    def rot(self, around, angle):
        """
        Rotate function for the Triangle class.
        Return the rotated Triangle with
        its updated l_of_elements.

        around: The rotation center.
        angle: Rotation amount in degrees.
        """
        from helperfuncs import rotate

        l_of_points = self.get_points_as_object()
        rotated_points = [rotate(i, around, angle) for i in l_of_points]

        l_of_elements = [i for i in self.l_of_elements if i is not self]
        #print(l_of_elements)

        for i in rotated_points:
            for k in l_of_elements:
                if isinstance(k, Point) and (i.name == k.name):
                    k.update_point(i, l_of_elements)

        return self.__class__(self.name, l_of_elements)

class Circle:
    """
    Circle element class. Constructs a circle by
    'center' and 'radius' or a triangle object.
    In latter, it constructs the circumcircle of the triangle given.

    First possibility:
    center: Center of the circle. Has to be a point object.
    radius: Radius of the circle. Has to be a segment object.

    Second possibility:
    tri: The triangle that will be used to get
    the three points that circumcircle passes.
    """
    def __init__(self, l_of_elements, center=None, radius=None, tri=None):
        if tri is None: # First case of usage.
            self.choice = 0
            self.center = center
            self.l_of_elements = l_of_elements
            self.radius = radius
            self.name = center.name + radius.name
        if isinstance(tri, Triangle): # Second case of usage.
            self.choice = 1
            self.tri = tri
            self.l_of_elements = l_of_elements
            self.l_of_points = tri.get_points_as_object()
            self.name = tri.name

    def __repr__(self):
        if self.choice == 0:
            return 'Circle({},{})'\
                    .format(self.center.name, self.radius.length)
        if self.choice == 1:
            l_of_names = [i.name for i in self.l_of_points]
            return 'Circle({}{}{})'.format(*l_of_names)

        return None

    def get_points_as_object(self):
        """
        Returns a list of points
        that constitutes this object.
        """
        if self.choice == 0:
            return [self.center, self.radius]
        if self.choice == 1:
            return self.l_of_points

        return None

    def sft(self, vector):
        """
        Shift function for the Circle class.
        Returns the shifted Circle with
        its updated l_of_elements.
        """
        from helperfuncs import shift


        if self.choice == 0:
            l_of_points = self.get_points_as_object()
            shifted_points = [shift(i, vector) for i in l_of_points]

            l_of_elements = [i for i in self.l_of_elements if i is not self]

            for i in shifted_points:
                for k in l_of_elements:
                    if isinstance(k, Point) and (i.name == k.name):
                        k.update_point(i, l_of_elements)

            c_name = self.center.name
            r_name = self.radius.name
            c_obj = [i for i in shifted_points if i.name == c_name][0]
            r_obj = [i for i in shifted_points if i.name == r_name][0]
            return self.__class__(l_of_elements, c_obj, r_obj)

        if self.choice == 1:
            new_tri = self.tri.sft(vector)
            return self.__class__(l_of_elements, new_tri)

        return None

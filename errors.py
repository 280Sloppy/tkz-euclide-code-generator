"""
errors.py
Contains error classes.
"""

class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class PointError(Error):
    """
    Exception that is raised for errors about the point dependencies.

    Attributes:
    msg: The message that explains which point that this exception
    is about.
    """

    def __init__(self, msg):
        self.msg = msg


class MultipleInstanceError(Error):
    """
    Exception that is raised if an element already exists.

    Attributes:
    msg: The message that says which element already exists.
    """

    def __init__(self, msg):
        self.msg = msg

class DimensionError(Error):
    """
    Exception that is raised if the dimensions
    of an object is not appropriate.
    """

    def __init__(self, msg):
        self.msg = msg

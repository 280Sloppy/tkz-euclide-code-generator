"""
matrix.py
Contains matrix class.
"""

from helperfuncs import lenchecker
from errors import DimensionError

class Vector:
    """
    Vector class.
    Constructs a vector.

    t_of_comp: Tuple of components of
    the vector.
    """
    def __init__(self, *t_of_comp):
        self.t_of_comp = t_of_comp
        self.length = len(t_of_comp)

    def __repr__(self):
        return 'Vector' + str(self.t_of_comp)

    def __len__(self):
        return self.length

    def __add__(self, vector):
        # Checking the type of object passed.
        if not isinstance(vector, Vector):
            raise TypeEror('Vector is expected\
                    but got {}'.format(type(vector)))
        # Checked.

        if not len(self.t_of_comp) == len(vector.t_of_comp):
            raise DimensionError('Expected {}-component vector\
                    but got {}-component one.'.\
                    format(len(self.t_of_comp),\
                    len(vector.t_of_comp)))

        new_lofcomp = list()
        for i in range(len(self.t_of_comp)):
            new_lofcomp.append(self.t_of_comp[i]+vector.t_of_comp[i])

        return Vector(*new_lofcomp)

    def __sub__(self, vector):
        # Checking the type of object passed.
        if not isinstance(vector, Vector):
            raise TypeError('Vector expected\
                    but got {}.'.format(type(vector)))
        # Checked.

        if not len(self.t_of_comp) == len(vector.t_of_comp):
            raise DimensionError('Expected {}-component vector\
                    but got {}-component one.'.\
                    format(len(self.t_of_comp),\
                    len(vector.t_of_comp)))

        new_lofcomp = list()
        for i in range(len(self.t_of_comp)):
            new_lofcomp.append(self.t_of_comp[i]-vector.t_of_comp[i])

        return Vector(*new_lofcomp)

    def __mul__(self, integer):
        # Checking the type of object passed.
        if not isinstance(integer, int):
            raise TypeError('Integer expected\
                    but got {}.'.format(type(integer)))
        # Checked.

        new_lofcomp = list()
        for i in range(len(self.t_of_comp)):
            new_lofcomp.append(integer * self.t_of_comp[i])

        return Vector(*new_lofcomp)


class Matrix:
    """
    Matrix class.
    Constructs a matrix.
    """
    def __init__(self, l_of_vectors):
        # Checking the uniformness
        # of the lengths of vectors.
        if not lenchecker(l_of_vectors):
            raise DimensionError('Dimensions of vectors\
                    are not uniform.')
        # Checked.

        self.dims = (len(l_of_vectors), len(l_of_vectors[0]))
        self.l_of_vectors = l_of_vectors

    def __repr__(self):
        reprlist = [i.t_of_comp for i in self.l_of_vectors]
        return 'Matrix:\n\t' + str(reprlist).\
                replace('),', ')\n\t')

    def __add__(self, matrix):
        new_lofvec = list()

        for i in range(len(self.l_of_vectors)):
            new_lofvec.append(self.l_of_vectors[i]+matrix.l_of_vectors[i])

        return Matrix(new_lofvec)

    def __mul__(self, matrix):
        # Checking the dimensions.
        if not self.dims[1] == matrix.dims[0]:
            print(self.dims[1], matrix.dims[0])
            raise DimensionError('Dimensions of the\
                    matrices does not match.')
        # Checked.

        new_lofvec = [[] for i in range(self.dims[0])]
        for m in range(self.dims[0]):
            for l in range(matrix.dims[1]):
                tobesummed = list()
                # So we're constructing the (AB)_{m,l}.
                # We need finally a loop for constructing it.
                for k in range(self.dims[1]):
                    # Noting self.dims[1] = matrix.dims[0]
                    s_vec = self.l_of_vectors[m]
                    v_vec = matrix.l_of_vectors[k]
                    tobesummed.append(s_vec.t_of_comp[k]*\
                            v_vec.t_of_comp[l])
                new_lofvec[m].append(sum(tobesummed))

        new_lofvec = [Vector(*i) for i in new_lofvec]
        return Matrix(new_lofvec)

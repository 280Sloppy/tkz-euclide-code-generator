"""
corefuncs.py
Contains core functions.
"""

from elements import Point
from elements import Segment
from elements import Angle
from elements import Triangle
from elements import Circle
from errors import PointError
from errors import MultipleInstanceError

def read(cli=None):
    """
    Takes the input of the user,
    splits it to commands and parameters
    """
    if cli is None:
        inp = input('input>')
        l_of_cmds = inp.split(' ')
        return l_of_cmds
    if isinstance(cli, str):
        l_of_cmds = cli.split(' ')
        return l_of_cmds

    return None


def interpret(l_of_cmds, l_of_elements):
    """
    Interprets the input of the user.

    l_of_cmds: List of the commands that will
    be interpreted. Typically comes from the return
    value of the 'read' function.

    l_of_elements: List of elements. Will be used
    for adding elements to it.
    """

    l_of_commands = ['point', 'segment', 'angle', 'triangle',\
            'circle', 'generate']

    if l_of_cmds[0] == 'point':
        # Note that (n,y) or (x, n) is not supported for now.
        # I don't see a way to benefit that type of information.
        # So point "point A 1" will return "A(None, None)" for example.
        try:
            l_of_elements.append(Point(l_of_cmds[1],\
                    l_of_elements, l_of_cmds[2], l_of_cmds[3]))
        except MultipleInstanceError as err:
            print(err)
        except IndexError:
            try:
                l_of_elements.append(Point(l_of_cmds[1], l_of_elements))
            except MultipleInstanceError as err:
                print(err)
            except IndexError:
                print('Lack of parameter. usage: point <name> x y')
    elif l_of_cmds[0] == 'segment':
        try:
            l_of_elements.append(Segment(l_of_cmds[1],\
                    l_of_elements, l_of_cmds[2]))
        except MultipleInstanceError as err:
            print(err)
        except PointError as err:
            print(err)
        except IndexError:
            try:
                l_of_elements.append(Segment(l_of_cmds[1], l_of_elements))
            except PointError as err:
                print(err)
            except MultipleInstanceError as err:
                print(err)
    elif l_of_cmds[0] == 'angle':
        l_of_elements.append(Angle(l_of_cmds[1], l_of_elements, l_of_cmds[2]))
    elif l_of_cmds[0] == 'triangle':
        try:
            l_of_elements.append(Triangle(l_of_cmds[1], l_of_elements))
        except PointError as err:
            print(err)
    elif l_of_cmds[0] == 'circle':
        tri_str = l_of_cmds[1]
        for i in l_of_elements:
            if isinstance(i, Triangle):
                if i.name == tri_str:
                    tri = i
        l_of_elements.append(Circle(l_of_elements, tri=tri))
#    elif l_of_cmds[0] == 'generate':
#        for i in l_of_elements:
#            if i.compare_names(l_of_cmds[1]):
#                generate(i)
    elif l_of_cmds[0] == 'exit':
        # exit()
        return False
    elif l_of_cmds[0] not in l_of_commands:
        print('Unrecognized Command \"{}\"'.format(l_of_cmds[0]))

    return None

# ------- GENERATE FUNCTIONS ------- #
def generate(*t_of_drawables, l_of_elements):
    """
    Code generation function. Calls
    child functions for each element.

    t_of_drawables: Tuple of drawable elements like
    Segment, Triangle, Circle.
    """

    # Definition of points.
    def_point(l_of_elements)
    # Definition of points.

    #print(t_of_drawables) # DEBUGGING

    # Drawing elements
    for i in t_of_drawables:
        print('Considering {}'.format(i))
        if isinstance(i, Segment):
            print('IN SEGMENT')
            gen_segment(i)
        elif isinstance(i, Triangle):
            print('IN TRIANGLE')
            gen_triangle(i)
        elif isinstance(i, Circle):
            print('IN CIRCLE')
            gen_circle(i)
    # Elements done.

    # Labeling points, segments.
    l_of_labelable = [i for i in l_of_elements if hasattr(i, 'label')]
    l_of_wblabeled = [i for i in l_of_labelable if i.wblabeled]
    gen_label(l_of_wblabeled)
    # Labeling points, segments etc.

# POINT DEFINITIONS.
def def_point(l_of_elements):
    """
    Definition of points.
    """
    # FIXME: Latex doesn't support scientific 'e' notation. We're handling it here.
    # '5' is the value that fltred function uses by default.
    from filerelated import wrap

    points = {i for i in l_of_elements if isinstance(i, Point)}
    with open('out.tex', 'a') as fileobj:
        for i in points:
            reduced_x = str(round(i.x_val, 5))
            reduced_y = str(round(i.y_val, 5))
            wrapped = wrap(i.name, '{')
            fileobj.write('\t')
            fileobj.write('\\tkzDefPoint({}, {}){}\n'\
                    .format(reduced_x, reduced_y, wrapped))

### SEGMENT ###
def gen_segment(segment):
    """
    Sub-generator for segment.
    """
    # \tkzDrawSegment(B,C) Example.
    from filerelated import spacer

    extremity = segment.get_points_name()

    spacer()
    with open('out.tex', 'a') as fileobj:
        fileobj.write('\t')
        fileobj.write('\\tkzDrawSegment({}, {})\n'\
                .format(extremity[0], extremity[1]))

### TRIANGLE ###
def gen_triangle(triangle):
    """
    Sub-generator for triangle.
    """
    from filerelated import spacer
    from filerelated import tri_name_init

    spacer()
    with open('out.tex', 'a') as fileobj:
        fileobj.write('\t')
        fileobj.write('\\tkzDrawPolygon({})\n'.format(tri_name_init(triangle)))

### CIRCLE ###
def gen_circle(circle):
    """
    Sub-generator for circle.
    """
    # FIXME: In first 'if', non-centric point of radius has to be choosen. See line 188.
    # Fixed but didn't tested.

    from filerelated import spacer

    if circle.choice == 0:
        radius = circle.radius
        extremity = radius.get_points_name()
        spacer()
        with open('out.tex', 'a') as fileobj:
            fileobj.write('\t')
            fileobj.write('\\tkzDrawCircle[radius]({}, {})\n'.\
                    format(*extremity))

    elif circle.choice == 1:
        l_of_points = circle.l_of_points
        l_of_names = [i.name for i in l_of_points]
        spacer()
        with open('out.tex', 'a') as fileobj:
            fileobj.write('\t')
            fileobj.write('\\tkzDrawCircle[circum]({},{},{})\n'.\
                    format(*l_of_names))

def gen_label(l_of_wblabeled):
    """
    Sub-generator for labels.
    """
    from filerelated import wrap
    from filerelated import spacer

    spacer()
    for i in l_of_wblabeled:
        if isinstance(i, Point):
            with open('out.tex', 'a') as fileobj:
                wrapped = wrap(i.name, '$', need_pairs=False)
                wrapped = wrap(wrapped, '{')
                fileobj.write('\t')
                fileobj.write('\\tkzLabelPoint({}){}\n'\
                        .format(i.name, wrapped))
        elif isinstance(i, Segment):
            # \tkzLabelSegment(A,B){$5$} Example.
            with open('out.tex', 'a') as fileobj:
                wrapped = wrap(i.name, '$', need_pairs=False)
                wrapped = wrap(wrapped, '{')
                fileobj.write('\t')
                fileobj.write('\\tkzLabelSegment({}, {}){}\n'\
                        .format(*i.p_letters, wrapped))

# ------- GENERATE FUNCTIONS ------- #
